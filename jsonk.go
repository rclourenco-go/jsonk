package jsonk

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"
	"unicode"
	"unicode/utf8"
)

type JsonToken struct {
	t     byte
	value interface{}
}

type JsonReader struct {
	content  *bytes.Reader
	ordering bool
	ordertag string
	pos      int
}

func NewToken(t byte, value interface{}) *JsonToken {
	return &JsonToken{t: t, value: value}
}

func isLimiter(c rune) bool {
	switch c {
	case '{', '}', '[', ']', ',', ':':
		return true
	default:
		return false
	}
}

func (this *JsonReader) NextChar() (rune, error) {
	r, c, e := this.content.ReadRune()
	this.pos += c
	return r, e
}

func (this *JsonReader) BackChar(k rune) {
	this.pos -= utf8.RuneLen(k)
	this.content.UnreadRune()
}

func (this *JsonReader) NextToken() (*JsonToken, error) {
	var k rune
	var err error
	for k, err = this.NextChar(); err == nil; k, err = this.NextChar() {
		switch k {
		case 'F', 'f', 'T', 't', 'N', 'n':
			return this.NextSpecial(k)
		case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '+', '-':
			return this.NextNumber(k)
		case '{', '}', '[', ']', ',', ':':
			return NewToken(byte(k), nil), nil
		case '\'', '"':
			return this.NextString(k)
		default:
			if !unicode.IsSpace(k) {
				return nil, fmt.Errorf("Invalid character at %d (%c)", this.pos, k)
			}
		}
	}
	return nil, err
}

func (this *JsonReader) NextSpecial(s rune) (*JsonToken, error) {
	var out bytes.Buffer
	out.WriteRune(s)
	for k, err := this.NextChar(); err == nil && !unicode.IsSpace(k); k, err = this.NextChar() {
		if isLimiter(k) {
			this.BackChar(k)
			break
		}
		out.WriteRune(k)
	}
	cmp := strings.ToLower(out.String())
	if strings.Compare(cmp, "null") == 0 {
		return NewToken('x', nil), nil
	} else if strings.Compare(cmp, "true") == 0 {
		return NewToken('x', true), nil
	} else if strings.Compare(cmp, "false") == 0 {
		return NewToken('x', false), nil
	}
	return nil, fmt.Errorf("Invalid token (%s) at %d", cmp, this.pos)
}

func (this *JsonReader) NextNumber(s rune) (*JsonToken, error) {
	var out bytes.Buffer
	out.WriteRune(s)
	for k, err := this.NextChar(); err == nil && !unicode.IsSpace(k); k, err = this.NextChar() {
		if isLimiter(k) {
			this.BackChar(k)
			break
		}
		out.WriteRune(k)
	}

	cmp := out.String()
	if strings.ContainsAny(cmp, ".eE") {
		v, e := strconv.ParseFloat(cmp, 64)
		if e != nil {
			return nil, e
		}
		return NewToken('n', v), nil

	} else {
		v, e := strconv.ParseInt(cmp, 10, 64)
		if e != nil {
			return nil, e
		}
		return NewToken('n', v), nil
	}
}

func (this *JsonReader) parseUnicode() (rune, error) {
	var r bytes.Buffer
	r.WriteString(`\u`)
	count := 0
	for k, err := this.NextChar(); err == nil; k, err = this.NextChar() {
		if isLimiter(k) || k == '"' || k == '\'' {
			this.BackChar(k)
			break
		}
		r.WriteRune(k)
		if count++; count >= 4 {
			break
		}
	}
	v, _, _, err := strconv.UnquoteChar(r.String(), 'u')
	return v, err
}

func (this *JsonReader) NextString(s rune) (*JsonToken, error) {
	var out bytes.Buffer
	var k rune
	var err error
	escape := false
	for k, err = this.NextChar(); err == nil; k, err = this.NextChar() {
		if k == '\\' && !escape {
			escape = true
			continue
		}
		if !escape && k == s {
			break
		}
		if escape {
			switch k {
			case 'b':
				out.WriteRune('\b')
			case 'f':
				out.WriteRune('\f')
			case 'n':
				out.WriteRune('\n')
			case 'r':
				out.WriteRune('\r')
			case 't':
				out.WriteRune('\t')
			case 'u':
				v, erru := this.parseUnicode()
				if erru == nil {
					out.WriteRune(v)
				}
			default:
				out.WriteRune(k)
			}
		} else {
			out.WriteRune(k)
		}
		if escape {
			escape = false
		}
	}
	if err != nil {
		return nil, err
	}
	return NewToken('s', out.String()), nil
}

func (this *JsonReader) ParseObject() (map[string]interface{}, error) {
	var k *JsonToken
	var err error

	out := make(map[string]interface{})

	st := 0
	var key string
	var value interface{}

	ordering := make([]string, 0, 2)

	for k, err = this.NextToken(); err == nil; k, err = this.NextToken() {
		if k.t == '}' {
			break
		}
		switch st {
		case 0:
			switch k.t {
			case 's':
				key = k.value.(string)
			default:
				return nil, fmt.Errorf("Expected key name at %d got %c", this.pos, k.t)
			}
			st = 1
		case 1:
			if k.t != ':' {
				return nil, fmt.Errorf("Expected : at %d", this.pos)
			}
			st = 2
		case 2:
			switch k.t {
			case 's':
				value = k.value.(string)
			case 'x':
				value = k.value
			case 'n':
				value = k.value
			case '[':
				av, ae := this.ParseArray()
				if ae != nil {
					return nil, ae
				}
				value = av
			case '{':
				ov, oe := this.ParseObject()
				if oe != nil {
					return nil, oe
				}
				value = ov
			default:
				return nil, fmt.Errorf("Unexpected Token at %d (%c)", this.pos, k.t)
			}
			out[key] = value
			if this.ordering {
				ordering = append(ordering, key)
			}
			st = 3
		case 3:
			if k.t != ',' {
				return nil, fmt.Errorf("Expected , at %d", this.pos)
			}
			st = 0
		}
	}
	if err != nil {
		return nil, err
	}
	if this.ordering {
		out[this.ordertag] = ordering
	}
	return out, nil
}

func (this *JsonReader) ParseArray() ([]interface{}, error) {
	var k *JsonToken
	var err error

	out := make([]interface{}, 0, 2)

	expectcomma := false
	for k, err = this.NextToken(); err == nil; k, err = this.NextToken() {
		if k.t == ']' {
			break
		}

		if expectcomma && k.t != ',' {
			return nil, fmt.Errorf("Expected , at %d", this.pos)
		} else if !expectcomma && k.t == ',' {
			return nil, fmt.Errorf("Unexpected , at %d", this.pos)
		}

		expectcomma = k.t != ','

		switch k.t {
		case 's':
			out = append(out, k.value)
		case 'x':
			out = append(out, k.value)
		case 'n':
			out = append(out, k.value)
		case '[':
			av, ae := this.ParseArray()
			if ae != nil {
				return nil, ae
			}
			out = append(out, av)
		case '{':
			ov, oe := this.ParseObject()
			if oe != nil {
				return nil, oe
			}
			out = append(out, ov)
		default:
			if k.t != ',' {
				return nil, fmt.Errorf("Unexpected Token at %d (%c)", this.pos, k.t)
			}
		}
	}
	if err != nil {
		return nil, err
	}
	return out, err
}

func Parse(json string, ordertag string) (interface{}, error) {
	ordering := ordertag != ""
	r := &JsonReader{
		content:  bytes.NewReader([]byte(json)),
		ordering: ordering,
		ordertag: ordertag,
	}
	for k, err := r.NextToken(); err == nil; k, err = r.NextToken() {
		if err != nil {
			break
		}
		switch k.t {
		case 's':
			return k.value, nil
		case 'x':
			return k.value, nil
		case 'n':
			return k.value, nil
		case '[':
			av, ae := r.ParseArray()
			return av, ae
		case '{':
			ov, oe := r.ParseObject()
			return ov, oe
		default:
			return nil, nil
		}
	}
	return nil, nil
}
