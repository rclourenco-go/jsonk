package main

import (
  "fmt"
  "gitlab.com/rclourenco-go/jsonk"
)

func main() {
  str := `{
    "world" : "hel\"lo",
    "xasdr":123.45,
    "array":
    [1,2,[3,4],5, 6, true],
    "special":null,
    "true is":true,
    "false is":false
    }`
  v,e := jsonk.Parse(str,"")
  if e != nil {
    fmt.Println(e)
  }
  fmt.Printf("%#v\n",v)

  v,e = jsonk.Parse(str,"_sorted_")
  if e != nil {
    fmt.Println(e)
  }
  fmt.Printf("%#v\n",v)
  
  vx := v.(map[string]interface{})
  if vx!=nil && vx["_sorted_"] != nil {
    for _,k := range vx["_sorted_"].([]string) {
      fmt.Printf(" %v \n", vx[k])
    }
  }
}
